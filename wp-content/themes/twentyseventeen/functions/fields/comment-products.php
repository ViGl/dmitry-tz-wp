<?php
/**
 * Created by PhpStorm.
 * User: gs
 * Date: 03.09.2018
 * Time: 19:15
 */

//TODO 3
function comment_output_comment_field() {
    ?>
    <div class="engraving-field">
        <input type="text" id="comment" name="comment" placeholder="comment" maxlength="10">
    </div>
    <?php
}

add_action( 'woocommerce_before_add_to_cart_button', 'comment_output_comment_field', 10 );
function add_comment_text_to_cart_item( $cart_item_data, $product_id, $variation_id ) {
    $comment_text = filter_input( INPUT_POST, 'comment' );

    if ( empty( $comment_text ) ) {
        return $cart_item_data;
    }

    $cart_item_data['comment'] = $comment_text;

    return $cart_item_data;
}

add_filter( 'woocommerce_review_order_after_submit', 'add_comment_text_to_cart_item', 10, 3 );


/**
 * @param $item_data
 * @param $cart_item
 * @return array
 */
function display_comment_text_cart($item_data, $cart_item ) {
    if ( empty( $cart_item['comment'] ) ) {
        return $item_data;
    }

    $item_data[] = array(
        'key'     => 'comment',
        'value'   => wc_clean( $cart_item['comment'] ),
        'display' => '',
    );

    return $item_data;
}
add_filter( 'woocommerce_get_item_data', 'display_comment_text_cart', 10, 2 );

function get_text_comment($item_data, $cart_item){
    $json = display_comment_text_cart( $item_data, $cart_item );

    return $json->description;
}



function add_comment_text_to_order_items( $item, $cart_item_key, $values, $order ) {
    if ( empty( $values['comment'] ) ) {
        return;
    }

    $item->add_meta_data( 'comment', $values['comment'] );
}

add_action( 'woocommerce_checkout_create_order_line_item', 'add_comment_text_to_order_items', 10, 4 );


