<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 04.09.2018
 * Time: 2:01
 */

function addProductFromUser()
{
    $name = $_POST['name'];
    $regular_price = (double)$_POST['regular_price'];
    $desc = $_POST['desc'];
    $short_desct = $_POST['short_desc'];

    if(strlen($name) > 150){
        $msg = ['status' => 'Наименование привышает количество символов!'];
    }elseif(empty($regular_price)){
        $msg = ['status' => 'Пустая цена!','color' => 'red'];
    }elseif(empty($desc)){
        $msg = ['status' => 'Пустое описание!', 'color' => 'red'];
    }

    $products = new WC_Product();
    $products->set_name($name);
    $products->set_regular_price($regular_price);
    $products->set_short_description($short_desct);

    $result = $products->save();
    if($result) {
        $msg = ['status' => "Товар под номером: " . $result . " Добавлен в систему!", 'color' => 'green'];
    }

    print json_encode($msg);
    wp_die();
}

add_action('wp_ajax_apfu', 'addProductFromUser');
add_action('wp_ajax_nopriv_apfu', 'addProductFromUser');
