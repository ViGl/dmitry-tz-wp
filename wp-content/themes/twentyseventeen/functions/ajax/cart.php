<?php
/**
 * Created by PhpStorm.
 * User: yamok
 * Date: 20.11.2017
 * Time: 17:05
 */

function addToCart()
{
    if(isset($_POST['product_id'])) {
        $id = $_POST['product_id'];
        $q = ($_POST['qty']) ? $_POST['qty'] : 1;
        if(WC()->cart->add_to_cart($id, $q)){
            $return = ['status' => 1];
        }
    }else{
        $return = ['status' => 1];
    }
    print  json_encode($return);
    wp_die();
}

add_action('wp_ajax_atc', 'addToCart');
add_action('wp_ajax_nopriv_atc', 'addToCart');
