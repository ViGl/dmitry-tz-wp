<?//TODO 5?>
<div class="row">
    <label for="name">Название*</label>
    <input type="text" name="name" id="name" required>
</div>
<div class="row">
    <label for="regular_price">Ценв*</label>
    <input type="number" name="regular_price" id="regular_price" required>
</div>
<div class="row">
    <label for="desc">Описание*</label>
    <textarea type="text" name="desc" id="desc" placeholder="Описание" required></textarea>
</div>
<div class="row">
    <label for="short_desc">Краткое описание</label>
    <textarea type="text" name="short_desc" id="short_desc" placeholder="Краткое описание"></textarea>
</div>
<div class="row result_text"></div>
<input type="submit" value="Добавить" class="add-product">

<script>
    $ = jQuery;
    $(function(){
        html = $('html');
        html.on('click', 'input.add-product', function(){
            error = false;

            $('input[type="text"],input[type="number"], textarea').each(function(){
                if(!$(this).val()){
                    error = true;
                    $(this).css('border', '1px solid red');
                }
            });
            if(error){
                $('.result_text').text('Ошибка добавления!');
            }
            $name = $('input[name="name"]').val();
            if($name.length > 150){
                $('.result_text').text('Наименование не должно быть больше 150 символов!');
                error = true
            }

            if(!error) {
                $('html').find('.result_text').text("Пожалуйста подождите... Добавление...");
                $.ajax({
                    method: "POST",
                    data: {
                        action: "apfu",
                        name: $('input[name="name"]').val(),
                        regular_price: $('input[name="regular_price"]').val(),
                        desc: $('textarea[name="desc"]').val(),
                        short_desc: $('textarea[name="short_desc"]').val(),
                    },
                    url: '/wp-admin/admin-ajax.php'
                }).done(function (data) {
                    json = JSON.parse(data);
                    console.log(json,  html.find('.result_text'), data.status);
                    html.find('.result_text').css('color', json.color);
                    html.find('.result_text').text(json.status);

                    html.find('input[type="text"], input[type="number"]').val("");
                    html.find('textarea').val("");
                });
            }
        })
    })
</script>

