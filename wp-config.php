<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'wordpress');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{i<m5X98Pu;x}JLIj-q?H+C:_h:*Y}.4t^]Wfp*GaC#M*COk(t($*r742V;cs<4?');
define('SECURE_AUTH_KEY',  'q_)O_x!6J]g726ZmgU9n#6<&SLkv~qNx^j9*EiIJZ6&2Y|SdFD$6_#_E@(r]lVeG');
define('LOGGED_IN_KEY',    'Xk<?[z ;8XV/^V7,20V7o{,!<TjH6erN(.(kh/bx}|FXSEns%pUw3{2f!lj+V_mp');
define('NONCE_KEY',        '(TaMCbdl#%ALREh+6u&@9CAs~N)#Ef]N(IDZx*^MJ<Zh)[cLN2T6tF{6;5|G0Rx&');
define('AUTH_SALT',        'h82Iz!A&]UkI|6tbx-`7[8`{mK-~{z&5$ktt/Vg>6<Z8#MaxeaMN,Szx/u1j<Z/L');
define('SECURE_AUTH_SALT', '$;u[n;Wn<hKkvVquRldcR4MW%$xHHZbNp!Dw1fP<G]kuOP]0;Jd0}2,e[:r#^(==');
define('LOGGED_IN_SALT',   '#D,eh`wKD@ywQ/3]<@Anbyn&Yw!!dn#+p  =jC@KW^l1A`MZ x?Gm>Y]IH?TTo:L');
define('NONCE_SALT',       '>aAJ%j8-`t) =I`!r{zifO,}_:+Dp!NS{GFD0Ypl2X&E5Pr6V_ip}cLwNHOXgTdH');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
